from django.apps import AppConfig


class ThaliaConfig(AppConfig):
    name = 'Thalia'
