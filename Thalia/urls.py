from django.urls import re_path
from django.conf import settings
from django.conf.urls.static import static  
from . import views

#url for app
urlpatterns = [
    re_path(r'^$', views.index, name='index'), 
    re_path(r'^skills/$', views.skills, name='skills')
]